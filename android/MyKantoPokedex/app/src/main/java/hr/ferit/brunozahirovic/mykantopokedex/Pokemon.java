package hr.ferit.brunozahirovic.mykantopokedex;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Pokemon {
    @SerializedName("pokemon")
    @Expose
    private List<APokemon> pokemon = null;

    public void setPokemon(List<APokemon> pokemon) {
        this.pokemon = pokemon;
    }

    public List<APokemon> getPokemon() {
        return pokemon;
    }
}
