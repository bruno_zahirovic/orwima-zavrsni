package hr.ferit.brunozahirovic.mykantopokedex;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;


import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter  extends RecyclerView.Adapter<RecyclerAdapter.PokemonViewHolder> {

    private List<APokemon> pokemon = new ArrayList<>();
    private OnItemClickListener mListener;

    @NonNull
    @Override
    public PokemonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);
        return new PokemonViewHolder(listItem,mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PokemonViewHolder holder, int position) {
        holder.setData(pokemon.get(position));
    }



    @Override
    public int getItemCount() {
        return this.pokemon.size();
    }

    public void setData(List<APokemon> data){
        for (APokemon pokemon : data) {
        }
        this.pokemon.clear();
        this.pokemon.addAll(data);
        notifyDataSetChanged();
    }

    public void addNewItem(APokemon pkmn,int position){
        if(pokemon.size()>=position){
            pokemon.add(position,pkmn);
            notifyDataSetChanged();
        }
    }


    public void removeItem(int position){
        if(pokemon.size()>position){
            pokemon.remove(position);
            notifyDataSetChanged();
        }
    }

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListner(OnItemClickListener listner) { mListener=listner;}

//viewHolder -----------------------------------------
    public static class PokemonViewHolder extends RecyclerView.ViewHolder{
        private TextView tvName;
        private TextView tvType;
        private ImageView ivPokemon;


        public PokemonViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tvPokemonName);
            tvType = itemView.findViewById(R.id.tvPokemonType);
            ivPokemon = itemView.findViewById(R.id.ivPokemonImage);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }

        public void setData(APokemon pokemon){
            tvName.setText(pokemon.getName() + "\n(#" + pokemon.getNum() + ")");
            tvType.setText(pokemon.getType().toString());
            Glide.with(ivPokemon.getContext())
                    .load(pokemon.getImg())
                    .override(225,225)
                    .into(ivPokemon);
            List<String> types = pokemon.getType();
            if(types.contains("Grass"))
                tvType.setTextColor(itemView.getResources().getColor(R.color.green));
            else if(types.contains("Fire"))
                tvType.setTextColor(itemView.getResources().getColor(R.color.fireRed));
            else if(types.contains("Water"))
                tvType.setTextColor(itemView.getResources().getColor(R.color.waterBlue));
            else
                tvType.setTextColor(itemView.getResources().getColor(R.color.black));
        }


    }

}
