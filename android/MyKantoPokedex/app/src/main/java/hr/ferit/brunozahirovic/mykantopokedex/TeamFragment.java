package hr.ferit.brunozahirovic.mykantopokedex;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.http.GET;

public class TeamFragment extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerViewTeamAdapter recyclerViewTeamAdapter;
    private SharedViewModel viewModel;
    private RecyclerViewTeamAdapter.OnTeamItemClickListener listener;
    private List<APokemon> team = new ArrayList<>(6);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_team, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialSetup(view);
        recyclerViewTeamAdapter.setOnTeamItemClickListener(new RecyclerViewTeamAdapter.OnTeamItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if(team.size()>0){
                    removeTeamMember(position);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        setPrefs();
        super.onDestroy();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel =  new ViewModelProvider(getActivity()).get(SharedViewModel.class);
        viewModel.getPokemon().observe(getViewLifecycleOwner(), new Observer<APokemon>() {
            @Override
            public void onChanged(APokemon pokemon) {
                addTeamMember(pokemon);
            }
        });
    } //getting data from SharedViewModel -- on change

    private void getPrefs(){
        SharedPreferences preferences = this.getActivity().getSharedPreferences("team", Context.MODE_PRIVATE);
        String prefs = preferences.getString("teamData","").trim();

        team = new ArrayList<>();
        if(prefs!=""){
            List<String> teamMembers=Arrays.asList(prefs.split("\n"));
            ArrayList<String> arrTeam = new ArrayList<>(teamMembers);
            Gson gson = new Gson();
            for (String item : arrTeam) {
                team.add(team.size(),gson.fromJson(item,APokemon.class));
            }
            Toast.makeText(getContext(),"Your team has been restored!",Toast.LENGTH_SHORT).show();
        }

    }

    private void setPrefs(){
        SharedPreferences preferences = this.getActivity().getSharedPreferences("team", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        Gson gson = new Gson();
        String prefsStr ="";
        for (APokemon pokemon: team) {
            prefsStr += gson.toJson(pokemon) + "\n";
        }
        prefsEditor.putString("teamData",prefsStr);
        prefsEditor.commit();
    }


    private void initialSetup(View v) {
        recyclerView = v.findViewById(R.id.rvTeam);
        recyclerViewTeamAdapter = new RecyclerViewTeamAdapter();
        recyclerView.setAdapter(recyclerViewTeamAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        getPrefs();
        recyclerViewTeamAdapter.setData(team);
    }

    public void addTeamMember(APokemon pokemon){
        if(team.size()<6){
            team.add(pokemon);
            recyclerViewTeamAdapter.addNewItem(pokemon,team.size()-1);
            Integer size = team.size();
            Toast.makeText(getContext(), "Added " + pokemon.getName() +" to your team!", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(getContext(), "Maximal team size is 6 pokemon!\nDelete some before adding more!", Toast.LENGTH_SHORT).show();
        }

    }

    public void removeTeamMember(int position){
        if(team.size()>0){
            Toast.makeText(getContext(),team.get(position).getName().toString() + " has been removed from your team!",Toast.LENGTH_SHORT).show();
            team.remove(position);
            recyclerViewTeamAdapter.removeItem(position);

        }
    }
}
