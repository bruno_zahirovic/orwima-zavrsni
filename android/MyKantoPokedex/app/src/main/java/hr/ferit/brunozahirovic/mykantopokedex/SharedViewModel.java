package hr.ferit.brunozahirovic.mykantopokedex;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SharedViewModel extends ViewModel { //allows easy fragment to fragment communication
    private MutableLiveData<APokemon> pokemonMutableLiveData = new MutableLiveData<>();

    public void setPokemonMutableLiveData(APokemon pokemon){
        pokemonMutableLiveData.setValue(pokemon);
    }

    public LiveData<APokemon> getPokemon(){
        return pokemonMutableLiveData;
    }
}
