package hr.ferit.brunozahirovic.mykantopokedex;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NextEvolution {

    @SerializedName("num")
    @Expose
    private String num;
    @SerializedName("name")
    @Expose
    private String name;

    public String getNum() {
        return num;
    }

    public String getName() {
        return name;
    }
}
