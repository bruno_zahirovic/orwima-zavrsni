package hr.ferit.brunozahirovic.mykantopokedex;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.AlignmentSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class TypeListFragment extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerAdapter recyclerAdapter;
    private List<APokemon> individualPokemon;
    private List<APokemon> displayPokemon = new ArrayList<>();
    private List<APokemon> filteredPokemon;
    private TextView tvType;
    private EditText etSearch;
    private String type;
    private SharedViewModel viewModel;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_list_type, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialSetup(view);
        recyclerAdapter.setOnItemClickListner(new RecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if(filteredPokemon.size()==0){
                    viewModel.setPokemonMutableLiveData(displayPokemon.get(position));
                }else{
                    viewModel.setPokemonMutableLiveData(filteredPokemon.get(position));
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());

            }
        }); //filter items to show

    }

    private void initialSetup(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerAdapter = new RecyclerAdapter();
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        tvType = view.findViewById(R.id.tvFragmentTitle);
        etSearch = view.findViewById(R.id.etSearchBar);
        if(getArguments()!=null){
            type=getArguments().getString("type");
            individualPokemon = getArguments().getParcelableArrayList("pokemon");
        }
        setupData(view);
        viewModel = new ViewModelProvider(getActivity()).get(SharedViewModel.class);
    }

    public void refreshET(){
        this.etSearch.setText("");
        filter("");
    }

    private void filter(String text) {
        filteredPokemon =  new ArrayList<>();

        for (APokemon aPokemon: displayPokemon) {
            if(aPokemon.getName().toLowerCase().contains(text.toLowerCase())){
                filteredPokemon.add(aPokemon);
            }
            recyclerAdapter.setData(filteredPokemon);
        }
    }

    public TypeListFragment(){}

    public void setupData(View view){
        filteredPokemon = new ArrayList<>();
        switch (type){
            case "GRASS":

                for (APokemon apokemon: individualPokemon) {
                    if(apokemon.getType().contains("Grass")){
                        displayPokemon.add(apokemon);
                    }
                }
                break;
            case "FIRE":
                for (APokemon apokemon: individualPokemon) {
                    if(apokemon.getType().contains("Fire")){
                        displayPokemon.add(apokemon);
                    }
                }
                break;
            case "WATER":
                for (APokemon apokemon: individualPokemon) {
                    if(apokemon.getType().contains("Water")){
                        displayPokemon.add(apokemon);
                    }
                }
                break;
            case "OTHER":
                for (APokemon apokemon: individualPokemon) {
                    if(!(apokemon.getType().contains("Water") || apokemon.getType().contains("Grass") || apokemon.getType().contains("Fire"))){
                        displayPokemon.add(apokemon);
                    }
                }
                break;
            default:
                displayPokemon = individualPokemon;
        }
        tvType.setText(type + " TYPE POKEMON");
        recyclerAdapter.setData(displayPokemon);
    }

    public void setItems(List<APokemon> pokemons, String type){
        this.type = type;
        this.individualPokemon=pokemons;
    }



}
