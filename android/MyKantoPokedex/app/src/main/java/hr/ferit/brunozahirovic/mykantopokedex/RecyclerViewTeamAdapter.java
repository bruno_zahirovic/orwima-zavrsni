package hr.ferit.brunozahirovic.mykantopokedex;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewTeamAdapter extends RecyclerView.Adapter<RecyclerViewTeamAdapter.PokemonViewHolder>{
        private RecyclerViewTeamAdapter.OnTeamItemClickListener listener;
        private List<APokemon> pokemon = new ArrayList<>(6);


    @NonNull
    @Override
    public PokemonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_member_item,parent,false);
        return new PokemonViewHolder(listItem, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull PokemonViewHolder holder, int position) {
        holder.setData(pokemon.get(position));
    }

    @Override
    public int getItemCount() {
        return this.pokemon.size();
    }

    public void setData(List<APokemon> data){
        for (APokemon pokemon : data) {
//            Log.d("Pokemon: ", pokemon.getImg());
        }
        this.pokemon.clear();
        this.pokemon.addAll(data);
        notifyDataSetChanged();
    }

    public void addNewItem(APokemon pkmn,int position){
        if(pokemon.size()>=position){
            pokemon.add(position,pkmn);
            notifyDataSetChanged();
        }
    }


    public void removeItem(int position){
        if(pokemon.size()>position){
            pokemon.remove(position);
            notifyDataSetChanged();
        }
    }

    public interface OnTeamItemClickListener{
        void onItemClick(int position);
    }

    public void setOnTeamItemClickListener(RecyclerViewTeamAdapter.OnTeamItemClickListener listner) { listener =listner;}


    public static class PokemonViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        private TextView tvType;
        private TextView tvWeaknesses;
        private TextView tvEvolvesInto;
        private ImageView ivPokemon;


        public PokemonViewHolder(@NonNull View itemView, RecyclerViewTeamAdapter.OnTeamItemClickListener listener) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvTeamMemberName);
            tvType = itemView.findViewById(R.id.tvTeamMemberType);
            tvWeaknesses = itemView.findViewById(R.id.tvWeakAgainst);
            tvEvolvesInto = itemView.findViewById(R.id.tvEvolvesInto);
            ivPokemon = itemView.findViewById(R.id.ivTeamMember);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }


        public void setData(APokemon pokemon){
            tvName.setText(pokemon.getName() + "\n(#" + pokemon.getNum() + ")");
            tvType.setText(pokemon.getType().toString());
            Glide.with(ivPokemon.getContext())
                    .load(pokemon.getImg())
                    .override(225,225)
                    .into(ivPokemon);
            tvWeaknesses.setText("Weaknesses: ");
            tvWeaknesses.append(pokemon.getWeaknesses().toString());
            tvEvolvesInto.setText("Evolves into: ");
            if(pokemon.getNextEvolution() != null){
                tvEvolvesInto.append(pokemon.getNextEvolution().get(0).getName() + "(#" + pokemon.getNextEvolution().get(0).getNum() + ")");
            }
            else
                tvEvolvesInto.append("-");

        }
    }
}
