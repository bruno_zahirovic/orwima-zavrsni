package hr.ferit.brunozahirovic.mykantopokedex;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class APokemon implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("num")
    @Expose
    private String num;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("type")
    @Expose
    private List<String> type = null;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("candy")
    @Expose
    private String candy;
    @SerializedName("candy_count")
    @Expose
    private Integer candyCount;
    @SerializedName("egg")
    @Expose
    private String egg;
    @SerializedName("spawn_chance")
    @Expose
    private Double spawnChance;
    @SerializedName("avg_spawns")
    @Expose
    private Double avgSpawns;
    @SerializedName("spawn_time")
    @Expose
    private String spawnTime;
    @SerializedName("multipliers")
    @Expose
    private List<Double> multipliers = null;
    @SerializedName("weaknesses")
    @Expose
    private List<String> weaknesses = null;
    @SerializedName("prev_evolution")
    @Expose
    private List<PrevEvolution> prevEvolution = null;
    @SerializedName("next_evolution")
    @Expose
    private List<NextEvolution> nextEvolution = null;

    //METHODS----

    protected APokemon(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        num = in.readString();
        name = in.readString();
        img = in.readString();
        type = in.createStringArrayList();
        height = in.readString();
        weight = in.readString();
        candy = in.readString();
        if (in.readByte() == 0) {
            candyCount = null;
        } else {
            candyCount = in.readInt();
        }
        egg = in.readString();
        if (in.readByte() == 0) {
            spawnChance = null;
        } else {
            spawnChance = in.readDouble();
        }
        if (in.readByte() == 0) {
            avgSpawns = null;
        } else {
            avgSpawns = in.readDouble();
        }
        spawnTime = in.readString();
        weaknesses = in.createStringArrayList();
    }

    public static final Creator<APokemon> CREATOR = new Creator<APokemon>() {
        @Override
        public APokemon createFromParcel(Parcel in) {
            return new APokemon(in);
        }

        @Override
        public APokemon[] newArray(int size) {
            return new APokemon[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public String getNum() {
        return num;
    }

    public String getName() {
        return name;
    }

    public String getImg() {
        return img;
    }

    public List<String> getType() {
        return type;
    }

    public List<String> getWeaknesses() {
        return weaknesses;
    }

    public List<NextEvolution> getNextEvolution() {
        return nextEvolution;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(num);
        dest.writeString(name);
        dest.writeString(img);
        dest.writeStringList(type);
        dest.writeString(height);
        dest.writeString(weight);
        dest.writeString(candy);
        if (candyCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(candyCount);
        }
        dest.writeString(egg);
        if (spawnChance == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(spawnChance);
        }
        if (avgSpawns == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(avgSpawns);
        }
        dest.writeString(spawnTime);
        dest.writeStringList(weaknesses);
    }
}
