package hr.ferit.brunozahirovic.mykantopokedex;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PokedexApi {
    @GET("pokedex.json")
    Call<Pokemon> getPokemonData();
}
