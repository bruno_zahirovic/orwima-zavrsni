package hr.ferit.brunozahirovic.mykantopokedex;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.ViewModel;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import android.content.pm.ActivityInfo;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity{
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private PageViewerAdapter mViewPagerAdapter;
    private Call<Pokemon> call;
    private Pokemon pokemon;
    private ArrayList<APokemon> individualPokemon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //don't allow orientation change
        initialSetup();
        setUpCall();
        setUpTabActions();


    }

    private void initialSetup(){
        mViewPager =findViewById(R.id.viewPager);
        mTabLayout = findViewById(R.id.tabLayout);
        mViewPagerAdapter = new PageViewerAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.POSITION_NONE);
        individualPokemon = new ArrayList<>();
        pokemon=new Pokemon();
    }

    private void setUpCall(){
        call = NetworkUtils.getApiInterface().getPokemonData();
        call.enqueue(new Callback<Pokemon>() {
            @Override
            public void onResponse(Call<Pokemon> call, Response<Pokemon> response) {
                if(response.isSuccessful() && response.body()!=null){
                    Log.d("onResponse",response.message());
                    pokemon = response.body();
                    individualPokemon.addAll(pokemon.getPokemon());
                    mViewPagerAdapter.setFragmentData(individualPokemon);
                    setUpPager();

                }
            }

            @Override
            public void onFailure(Call<Pokemon> call, Throwable t) {
                Log.d("Failure",t.getMessage());
                Toast.makeText(MainActivity.this,"Error!\nCheck your internet connection and try again!",Toast.LENGTH_LONG).show();
                setUpPager();
            }
        });
    }

    private void setUpTabActions(){
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                hideKeyboard(MainActivity.this);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Fragment fragment = (Fragment) mViewPagerAdapter.instantiateItem(mViewPager,tab.getPosition());
                if(fragment instanceof TypeListFragment){
                    fragment = (TypeListFragment) fragment;
                    ((TypeListFragment) fragment).refreshET();
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setUpPager() {
        mViewPager.setAdapter(mViewPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        TabLayout.Tab tab = mTabLayout.getTabAt(0);
        tab.select();
        mViewPager.setOffscreenPageLimit(6);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }



}