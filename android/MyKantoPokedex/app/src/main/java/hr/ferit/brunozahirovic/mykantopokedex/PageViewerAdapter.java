package hr.ferit.brunozahirovic.mykantopokedex;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentFactory;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class PageViewerAdapter extends FragmentStatePagerAdapter {
    private static final int NUM_PAGES = 6;
    private ArrayList<APokemon> pokemon = new ArrayList<>();

    private ArrayList<Fragment> fragments = new ArrayList<>();

    public PageViewerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("pokemon",pokemon);
        switch (position){
            case 0:
                fragment = new StartPageFragment();
                break;
            case 1:
                fragment = new TypeListFragment();
                bundle.putString("type","GRASS");
                break;
            case 2:
                fragment = new TypeListFragment();
                bundle.putString("type","FIRE");
                break;
            case 3:
                fragment = new TypeListFragment();
                bundle.putString("type","WATER");
                break;
            case 4:
                fragment = new TypeListFragment();
                bundle.putString("type","OTHER");
                break;
            case 5:
                Bundle bundle1 = new Bundle();
                fragment =  new TeamFragment();

        }
        fragment.setArguments(bundle);
        fragments.add(fragment);
        return fragment;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Starters";
            case 1:
                return "Grass types";
            case 2:
                return "Fire types";
            case 3:
                return "Water types";
            case 4:
                return "Other types";
            case 5:
                return "My team";
            default:
                return "#" + (position + 1);
        }
    }


    public void setFragmentData(ArrayList<APokemon> pokemon){
            this.pokemon=pokemon;
    }

}
